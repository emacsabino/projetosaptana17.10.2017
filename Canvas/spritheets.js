function Spritesheet(context,imagem,linhas,colunas){
	
	this.context = context;
	this.imagem = imagem;
	this.linhas = linhas;
	this.colunas = colunas;
	this.intervalo = 0;
	this.linha=0;
	this.coluna=0;
    this.fimDoCiclo = null;
}

Spritesheet.prototype = {
	proximoQuadro: function(){
		if(this.coluna<this.colunas-1){
			
			this.coluna++;
		}else{
			this.coluna=0;
            
            if (this.fimDoCiclo) this.fimDoCiclo();
		}
		
		
		var agora = new Date().getTime();
		
		if(!this.ultimoTempo) this.ultimoTempo = agora;
		
		if(agora - this.ultimoTempo<this.intervalo) return;
		
		if(this.coluna<this.colunas-1){
		 this.coluna++;
		}else{
			this.coluna=0;
		}
		this.ultimoTempo = agora;
	},
	desenhar: function(x,y){
		var larguraQuadro = this.imagem.width/this.colunas;
		var alturaQuadro = this.imagem.height/this.linhas;
		this.context.drawImage(
			
			this.imagem,
			larguraQuadro*this.coluna,
			alturaQuadro*this.linha,
			larguraQuadro,
			alturaQuadro,
			x,
			y,
			larguraQuadro,
			alturaQuadro
			
		);
		
		
		
		
	}
}
