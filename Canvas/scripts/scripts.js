/**
 * @author Nibble
 */




$(document).ready(function(e){
	
 
    
    
    var canvas = document.getElementById("primeiro");
    var context = canvas.getContext('2d');
    
    var x = 200;
    var y =500;
    var raio = 5;
    var anterior = new Date().getTime();
    requestAnimationFrame(mexerBola);
    
    
   
    
    function mexerBola(){
    var agora = new Date().getTime();
        
    var decorrido = agora - anterior;
    
    context.clearRect(0,0,canvas.width,canvas.height);
        
    context.beginPath();
    context.arc(x,y,raio,0,Math.PI*2);
    context.fill();
        
    var velocidade = 600;
    y-=velocidade * decorrido/1000;
        
    anterior = agora;
        
    requestAnimationFrame(mexerBola);
}
	
	
});
