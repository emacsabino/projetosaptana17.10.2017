<?php
include_once "app.mensagensfeed/operacoes_paginas.php";
session_start();
$valor = $_SESSION['validador'];


if($valor!='logado'){
	abrirPaginaNaoLogado();
	
}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>admin</title>
		<meta name="description" content="">
		<meta name="author" content="Nibble">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		
         <script src="scripts/jquery-3.1.0.js"></script>
         
       <!--
       	
       	 <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
       -->
         
         <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	
	<script src="scripts/bootstrap.min.js"></script>
         
         
         
         <link rel="stylesheet" href="css/admin.css">
          <link rel="stylesheet" href="editorText/editor.css">
          <script src="editorText/editor.js"></script>
		  <script src="scripts/script_admin.js"></script>
	</head>

	<body>
		<div id="conteiner_pagina">
			<div  id="opcoes_usuario_log">
            	<div id="conteiner_usuario">
            		<div class="opcoes2">RYADH</div>
            	<div class="opcoes2" id="opcoes_logoff">
            	SAIR
               </div>
            	</div>
            	
            </div>
			<div id="menu_adm">
				<div class="opcoes" id="opcoes_menu">
            	<div id="conteiner_opcoes_menu">☰ <span id="palavra_menu">MENU</span></div>
            	<h4 class="titulo_menus">POSTAGENS</h4>
            	<ul class="lista">
            		<li class="lis2" id="nova_postagem">NOVA</li>
            		<li class="lis2">EDITAR/EXCLUIR</li>
            		
            	</ul>
            	<h4>BANNERS</h4>
            	<ul class="lista">
            		<li class="lis2">INSERIR</li>
            		<li class="lis2">EXCLUIR</li>
            	</ul>
            	<h4>APLICATIVOS</h4>
            	<ul class="lista">
            		<li class="lis2">UPLOAD</li>
            		<li class="lis2">EXCLUIR</li>
            		
            	</ul>
            	
            	
            </div>
            <div class="opcoes" id="opcoes_titulo">
            	<div id="conteiner_titulo">
            		ADMINISTRAÇÃO
            	</div>
            </div>
            
            
			 </div>
            
			<div class="corpo" id="principal">
				<div class="campo_acao" id="enviar_post">
					<fieldset>
						<div id="menu_postagem">
							<div id="menu_postagem_conteiner">
							</div>
						</div>
						
						<legend>NOVA POSTAGEM</legend>
						<form name="postagem_nova">
							<div id="tamanho">
								 
						</div>
							<p><input name="titulo" placeholder="TÍTULO" id="titulo" type="text"></p>
							
							<p><textarea id="txtEditor"></textarea></p>
							<input name="btnEnviar" id="btn_enviar" value="ENVIAR" type="submit">
							<input type="reset" value="LIMPAR">
						</form>
					</fieldset>
					
					
				</div>

			</div>

			<footer>
				
			</footer>
		</div>
	</body>
</html>
