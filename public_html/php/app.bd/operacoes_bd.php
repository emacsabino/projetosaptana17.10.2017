<?php

function efetuarLoginAdministrador($conexao,$usuario,$senha){
	$retorno = false;
	try{
	$stmt = $conexao->prepare("SELECT * FROM administradores WHERE nome = ? AND senha = ?"); 
$stmt->bindParam(1, $usuario, PDO::PARAM_STR); 
$stmt->bindParam(2, $senha, PDO::PARAM_STR); 


// Executando statement 
$stmt->execute(); 

// Obter linha consultada 
$obj = $stmt->fetchObject(); 

// Se a linha existe: indicar que esta logado e encaminhar para outro lugar 
if ($obj) {
	$retorno = true;
	
}
	}catch(Exception $e){
		
		echo $e;
		
	}
	
	return $retorno;
}

function cadastrarAdministrador($conexao){
	$retorno = FALSE;
	try{
		$nomeUser = "admin";
		$senhaUser = "admin";
		$permissaoUser = "1";
		if(buscarNome($nomeUser)==FALSE){
  $stmt = $conexao->prepare("INSERT INTO administradores(nome, senha,permissao) VALUES (?,?,?)"); 
  $stmt->bindParam(1, $nomeUser, PDO::PARAM_STR); 
  $stmt->bindParam(2, $senhaUser, PDO::PARAM_STR); 
  $stmt->bindParam(3, $permissaoUser, PDO::PARAM_STR); 
  
  $stmt->execute();
	$retorno = TRUE;
		}	
	}catch(PDOException $a){
	    echo $a;
	}catch(Exception $e){
		echo $e;
	}
	
	return $retorno;
}

function buscarNome($nome){
	$retorno = FALSE;
	$pdo=conectar();
	$filiados=array();
	try{
		
		
	   
	 $nomeF=$nome;
		
	
	
  $stmt = $pdo->prepare("SELECT  nome FROM administradores WHERE nome=?;"); 
  $stmt->bindParam(1, $nomeF,PDO::PARAM_STR);
  $stmt->execute();
		$obj = $stmt->fetchObject(); 
	if($obj){
		$retorno=TRUE;
	}
	}catch(PDOException $erro){
		echo("ocorreu um erro");
	header('Location: index3.html');
	}
	
   return $retorno;
}

?>