<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Install</title>
		<meta name="description" content="">
		<meta name="author" content="Nibble">
		
		<script type="text/javascript" src="nible_editor/js/jquery-3.1.0.js"> </script>
		
		<link rel="stylesheet" href="css/install.css">
	
	</head>

	<body>
		<div class="caixas" id="title">Configuração do Banco</div>
		<div class="caixas" id="form">
			
			<form action="php/install_action.php" method="post">
				<p>	<input class="campos" name="nome_host" placeholder="Host" type="text"> </p>
				<P><input class="campos" name="nome_banco" type="text" placeholder="Nome do Banco"></P>
				<p>	<input class="campos" name="nome_usuario" placeholder="Usuário" type="text"> </p>
				<P><input class="campos" name="nome_senha" type="text" placeholder="Senha"></P>
				<p><input type="submit" name="btn_criar_tabelas" value="CRIAR" /></p>
			</form>
				
		</div>
	</body>
</html>